Brazil 2014 World Cup API
=========================

This API provides endpoints to query the groups and fixtures for the 2014
World Cup. The response format for all calls is JSON :).

A front end web app that uses this api can be found 
[here](http://brazil2014.kodefoundry.io).

Countries
---------

A list of countries involved in the World Cup finals can be request using;

    GET /api/countries
    
To get detailed information about a country, make a request to the following
endpoint;

    GET /api/country/:id
    
Where ```:id``` is the country name e.g. ```GET /api/country/Spain```. You
will get all the fixtures for that country along with information about which
group they belong to.
    
    
Groups
------

A list of groups can be queried using;

    GET /api/groups
    
The JSON object key is the group ID who's value is an array of countries in that
group.
    
Detailed information about a group can be gained using the following endpoint;

    GET /api/group/:id
    
Where ID should match a valid group (A - H). The response will provide the
countries within that group along with all their fixtures.


Fixtures
--------

The following request will give you all the fixtures for the group stages;

    GET /api/fixtures
    
These will be in date order (earliest first). There are multiple games on each
fixture day, so you may want to group the fixtures by day. You can do this
using the ```group``` query parameter;

    GET /api/fixtures?group=1
    