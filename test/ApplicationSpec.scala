import org.specs2.mutable._
import org.specs2.runner._
import org.junit.runner._

import play.Logger
import play.api.libs.json._
import play.api.test._
import play.api.test.Helpers._

/**
 * Add your spec here.
 * You can mock out a whole application including requests, plugins etc.
 * For more information, consult the wiki.
 */
@RunWith(classOf[JUnitRunner])
class ApplicationSpec extends Specification {

  "Application" should {

    "send 404 on a bad request" in new WithApplication{
      route(FakeRequest(GET, "/boum")) must beNone
    }

    "show a list of countries" in new WithApplication {

      val request = FakeRequest(GET, "/api/countries")
      val result = controllers.Api.countries()(request)

      status(result) must equalTo(OK)
      contentType(result) must beSome("application/json")

      val body: String = contentAsString(result)
      var json = Json.parse(body).as[List[String]]

      json.length mustEqual 32
    }

    "show a list of fixtures" in new WithApplication {

      val request = FakeRequest(GET, "/api/fixtures")
      val result = controllers.Api.fixtures()(request)

      status(result) must equalTo(OK)
      contentType(result) must beSome("application/json")

      val body: String = contentAsString(result)
      val json = Json.parse(body).as[List[JsObject]]

      json.length mustEqual 48
    }
  }
}
