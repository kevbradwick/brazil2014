(function(){

    var app = angular.module("brazil2014", ['ngRoute']);

//    app.config(['$routeProvider'], function($routeProvider){
//
//        $routeProvider.when('/countries', {
//            templateUrl: 'templates/countries.html',
//            controller: 'BrazilController'
//        })
//        .otherwise({
//            templateUrl: 'templates/index.html',
//            controller: 'BrazilController'
//        })
//    });
//
    app.factory('apiService', function($http){

        return {
            getGroups: function() {
                return $http.get('/api/groups')
                    .then(function(result){
                        return result.data;
                    });
            },
            getCountryInfo: function(name) {
                return $http.get('/api/country/' + encodeURI(name))
                    .then(function(result){
                        return result.data;
                    });
            }
        }
    });

    app.controller('GroupsController', function($scope, apiService){

        apiService.getGroups().then(function(groups){

            $scope.groups = groups;
        });
    });

    app.controller('CountryController', function($scope, apiService, $routeParams){

        apiService.getCountryInfo($routeParams.id).then(function(country){
            $scope.about = country;
        });
    });

    app.config(function($routeProvider) {

        $routeProvider.when('/', {
            templateUrl: '/assets/pages/index.html',
            controller: 'GroupsController'
        })
        .when('/country/:id', {
            templateUrl: '/assets/pages/country.html',
            controller: 'CountryController'
        });
    });

}());