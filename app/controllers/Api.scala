package controllers

import models.{Fixture, Group}
import play.api.mvc._
import play.api.libs.json._
import play.Logger
import scala.collection.mutable.ListBuffer

object Api extends Controller {

  /**
   * Returns a list of all the groups and the countries within each group
   * @return
   */
  def groups = Action {

    Ok(JsObject(
      Group.all.map(g => g.id -> JsArray(g.countries.map(c => JsString(c))))
    )).withHeaders(
        ACCESS_CONTROL_ALLOW_ORIGIN -> "*",
        CACHE_CONTROL -> "public, max-age=86400"
      )
  }

  /**
   * Returns countries and fixtures for a group
   *
   * @param id the group id
   * @return
   */
  def group(id: String) = Action {

    Group.all.find(g => g.id == id.toUpperCase) match {
      case Some(g: Group) =>
        Ok(JsObject(
          Seq(
            "id" -> JsString(g.id.toUpperCase),
            "countries" -> JsArray(g.countries.map(c => JsString(c))),
            "fixtures" -> JsArray(g.fixtures.map(f => JsObject(Seq(
              "place" -> JsString(f.place),
              "country_1" -> JsString(f.countries(0)),
              "country_2" -> JsString(f.countries(1)),
              "stadium" -> JsString(f.stadium),
              "date" -> JsString(f.date.toString)
            ))))
          )
        )).withHeaders(
            ACCESS_CONTROL_ALLOW_ORIGIN -> "*",
            CACHE_CONTROL -> "public, max-age=86400"
          )

      case _ =>
        NotFound(JsObject(Seq(
          "error" -> JsBoolean(true),
          "message" -> JsString("Unknown Group")
        )))
    }
  }

  /**
   * A list of all the countries in the World Cup
   * @return
   */
  def countries = Action {

    val countries = Group.all.map(g => g.countries).flatMap(c => c)

    Ok(JsArray(countries.map(c => JsString(c)))).withHeaders(
      ACCESS_CONTROL_ALLOW_ORIGIN -> "*",
      CACHE_CONTROL -> "public, max-age=86400"
    )
  }

  /**
   * Display information about a country
   *
   * @param id the country id
   * @return
   */
  def country(id: String) = Action {

    Group.all.find(g => g.countries.contains(id)) match {
      case Some(g: Group) =>

        // this group's fixtures
        val fixtures = g.fixtures.filter(f => f.countries.contains(id))

        Ok(JsObject(Seq(
          "name" -> JsString(id),
          "group" -> JsString(g.id),
          "fixtures" -> JsArray(fixtures.map(f => JsObject(Seq(
            "place" -> JsString(f.place),
            "country_1" -> JsString(f.countries(0)),
            "country_2" -> JsString(f.countries(1)),
            "stadium" -> JsString(f.stadium),
            "date" -> JsString(f.date.toString)
          ))))
        ))).withHeaders(
            ACCESS_CONTROL_ALLOW_ORIGIN -> "*",
            CACHE_CONTROL -> "public, max-age=86400"
          )

      case _ =>
        NotFound(JsObject(Seq(
          "error" -> JsBoolean(true),
          "message" -> JsString("Unknown country")
        )))
    }
  }

  /**
   * Display a list of all the fixtures for the group stages
   *
   * @return
   */
  def fixtures(group: Boolean) = Action { request =>

    val fixtures = Group.all.map(g => g.fixtures).flatMap(f => f)

    group match {

      // group by date
      case true =>

        var lst = new ListBuffer[Seq[Fixture]]()

        fixtures.groupBy(f => f.date.dayOfYear()).foreach { case(_, fixList) =>
          lst += fixList
        }

        Ok(JsArray(lst.map(seq => JsArray(seq.map(f => JsObject(Seq(
          "place" -> JsString(f.place),
          "country_1" -> JsString(f.countries(0)),
          "country_2" -> JsString(f.countries(1)),
          "stadium" -> JsString(f.stadium),
          "date" -> JsString(f.date.toString)
        ))))))).withHeaders(
          ACCESS_CONTROL_ALLOW_ORIGIN -> "*",
          CACHE_CONTROL -> "public, max-age=86400"
        )

      // standard list
      case _ =>
        Ok(JsArray(fixtures.map(f => JsObject(Seq(
          "place" -> JsString(f.place),
          "country_1" -> JsString(f.countries(0)),
          "country_2" -> JsString(f.countries(1)),
          "stadium" -> JsString(f.stadium),
          "date" -> JsString(f.date.toString)
        ))))).withHeaders(
            ACCESS_CONTROL_ALLOW_ORIGIN -> "*",
            CACHE_CONTROL -> "public, max-age=86400"
          )
    }
  }
}
