package models

import play.api.Play.{current, resourceAsStream}
import scala.xml.{NodeSeq, XML}
import play.Logger
import org.joda.time.format.DateTimeFormat
import org.joda.time.DateTime


case class Fixture(fixture: scala.xml.Node) {

  def place: String = (fixture \ "@place").text

  def group: String = (fixture \ "@group").text

  def stadium: String = (fixture \ "@stadium").text

  // fixture date
  def date: DateTime = {

    val fr = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss")
    fr.parseDateTime((fixture \ "@date").text)
  }

  // the countries in this fixture
  def countries: Seq[String] = fixture \\ "country" map(c => c.text)

  // display title country(0) v country(1)
  def title: String = this.countries mkString " v "
}


case class Group (group: scala.xml.Node) {

  // the group id
  def id: String = (group \ "@id").text

  // a sequence of country names
  def countries: Seq[String] = (group \\ "country").map(node => node.text)

  // the fixtures for this group
  lazy val fixtures: Seq[Fixture] = {

    resourceAsStream("resources/fixtures.xml") match {
      case Some(is: java.io.InputStream) =>
        Logger.debug("Loaded fixtures.xml")
        val fixtures = (XML.load(is) \\ "fixture").filter(n => (n \ "@group").text == this.id)
        fixtures.map(ns => Fixture(ns)).sortBy(f => f.date.getMillis)
      case _ => Seq()
    }
  }
}


object Group {

  /**
   * Return all the groups and countries.
   *
   * @return
   */
  def all: Seq[Group] = {

    resourceAsStream("resources/groups.xml") match {
      case Some(is: java.io.InputStream) =>
        Logger.debug("Loaded groups.xml")
        val groups = (XML.load(is) \\ "group").map(group => Group(group))
        groups
      case _ =>
        Logger.error("Unable to load groups.xml")
        Seq()
    }
  }
}